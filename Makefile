# 'make'		run project
# 'make build'		build project
# 'make clean'		clean project
#

# define the C# compiler to use
CC = dotnet

# define any compile-time flags
CFLAGS = --project src/xyz.csproj

DEFAULT: 
	@-clear && $(CC) run $(CFLAGS) 

build: 
	@-clear && $(CC) build

clean: 
	@-rm -r src/bin
	@-rm -r src/obj
