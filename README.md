# XYZ Shipping Co.
```
Semester 2, Assignment I
ONT4202  [4206] 
Nelson Mandela University (2020)
Design Pattern : Iterator, Composite, Poxy
```
Team of Experts

```
Ice Mentation
Chuck Norris
Bhuda K-dash
```

## Table of Contants
<!-- vim-markdown-toc GitLab -->

* [Introduction](#introduction)
	* [Outline](#outline)
	* [UML](#uml)
* [Documentation](#documentation)
	* [Abstraction](#abstraction)
	* [Iterator Pattern](#iterator-pattern)
	* [Composite Pattern](#composite-pattern)
	* [Proxy Pattern](#proxy-pattern)
	* [Windows Communincation Foundation](#windows-communincation-foundation)
	* [Application](#application)

<!-- vim-markdown-toc -->

## Introduction

Console Application to facilitate the shipping process of company **XYZ Shipping**, for its clients - **ABC Products**, **DEF Foods**, **GHI Parts**.  

Using the **Iterator** pattern, the application is able to combine the three lists of products requested by these businesses, into one object, while providing means for the elements of this _aggregated_ list to be accessed sequentially, while implementation remains encapsulated.

The next part of application is the _shipping process_ of the list of these products requested by these companies.  

<br/><br/><br/>
<br/><br/><br/>
<br/><br/><br/>



### Outline 
On the part of the application, this is put to practice by means of the **Composite** pattern, and the _eight stages_ of the shipping process are denoted with the **eight levels** in the pattern hierarchy, as fllows.  



![Outline](https://cloud.owncube.com/apps/gallery/preview.public/60187729?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  

This technique affords the application the 3rd option in the main menu, which displays a list of timestamps of the entire shipping process of the selected shipment.

![Option 3](https://cloud.owncube.com/apps/gallery/preview.public/60380420?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM) 


<br/><br/><br/><br/>
<br/><br/>


### UML
The following is a representation of the solution back-end in a **UML diagram**.  

![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/60186619?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  
**Remote Repository** : [https://cosurmyne@bitbucket.org/nelsonmetro/ont4202-a1y20.git](git clone https://cosurmyne@bitbucket.org/nelsonmetro/ont4202-a1y20.git)  

<br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/>


## Documentation
The application kicks start with the **Product** class, to house items being requested by the three companies.  

```cs
    public class Product {
        private string desc;
        private double weight;
        public Product(string desc, double weight) {
            this.desc = desc;
            this.weight = weight;
        }
		public string Description { get{ return desc; } }
		public double Weight { get{ return weight; } }
        public override string ToString() {
			return String.Format("{0, -20}: {1}kg", Description, Weight);
        }
    }
```

Adding a list of products to that specific shipping happens immediately after creating new instance of shipment.

![Product](https://cloud.owncube.com/apps/gallery/preview.public/60381432?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  

Users also have the ability to check products being shipped.

![Query](https://cloud.owncube.com/apps/gallery/preview.public/60416326?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  






### Abstraction
The three businesses that make requests are aggregated to their respective classes, implement a mutual abstract class.  

```cs
    public abstract class Aggregate
    {
        protected List<Product> _items = new List<Product>();

        public abstract Iterator CreateIterator();
        public abstract void Add(Product o);

        public double Weigh()
        {
            double weight = 0;
            foreach (var item in _items)
            {
                weight += item.Weight;
            }
            return weight;
        }
        public byte Count()
        {
            return (byte)_items.Count;
        }

        public void Display()
        {
            foreach (var item in _items)
            {
                Console.WriteLine("{0}", item.ToString());
            }
            Console.ReadKey();
        }
    } 
```  

This abstract class is then linked to an interface to handle the actual iterating.  

```cs
    public interface Iterator
    {
        Product First();
        Product Next();
        Product Current();
        bool IsDone();
    }
``` 

We create a **client** to consume output from both, the Iterator and Composite pattern, and also handle requests to and from the **Windows Communincation Foundation** using the **Proxy Pattern**, and it defines the following interface.  

```cs
    public interface Base
    {
	    void GetList();
	    void TimeStamps();
	    void Create();
	    void Query();
    }
```
<br/><br/><br/><br/>
To handle shipping, we have an abstract class to serve as a component root to initialize the hierarchy for the Composite pattern.  

```cs
    public abstract class Shipping
    {
        public abstract string Description { get; }
        public abstract DateTime Time { get; }
        public abstract Aggregate Records { get; }
        public virtual byte Stage { get; }

        public abstract void DisplayInfo(bool timestamps, bool root = false);

        public virtual Shipping this[int index]
        {
            get
            {
                throw new System.NotSupportedException("Sorry, not today.");
            }
        }

        public virtual void Add(Shipping item)
        {
            throw new System.NotSupportedException("Nothing to Add");
        }

        public virtual void Remove(Shipping item)
        {
            throw new System.NotSupportedException("Nothing to Remove");
        }

        public virtual int Count()
        {
            throw new System.NotSupportedException("Nothing to Count.");
        }

	public virtual void ListProducts()
	{
            throw new System.NotSupportedException("Nothing to See Here.");
	}

        public virtual string[] Menu()
	{
            throw new System.NotSupportedException("Nothing to See Here.");
	}
  }
```


The **WCF** defines the following endpoints in its **ServiceContract**  

```cs
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        string[] Pavillion();
        [OperationContract]
        string[] Prompt();
        [OperationContract]
        string[] AddMore();
        [OperationContract]
        string[] Stage();
    }
```
<br/><br/>
### Iterator Pattern
All of the three businesses implement the Aggregate abstract class in the following fashion.  

```cs
    public class ABC : Aggregate
    {
        public Product this[int index]
        {
            get { return _items[index]; }
        }

        public override void Add(Product o)
        {
            _items.Add(o);
        }

        public override Iterator CreateIterator()
        {
            return new IteratorABC(this);
        }

	public override string ToString()
	{
		return "ABC Products";
	}
  }
``` 

Same story for the Iterators; they implement their respective interface in the same fashion, as follows.  

```cs
    public class IteratorABC : Iterator
    {
        private ABC _aggregate;
        private int _position;

        public IteratorABC(ABC aggregate)
        {
            _aggregate = aggregate;
            _position = 0;
        }

        public Product Current()
        {
		if (_position < _aggregate.Count()) return _aggregate[_position];
		return null;
        }

        public Product First()
        {
            _position = 0;
	    return Current();
        }

        public bool IsDone()
        {
		return _position >= _aggregate.Count();
        }

        public Product Next()
        {
		_position++;
		return Current();
        }
    }
```
<br/>
### Composite Pattern
Remember the **Shipping** abstract class above?  
It is implemented with **Process** class to serve as a Composite, as follows;-  

```cs
    public class Process : Shipping
    {
        // fields...

        private List<Shipping> _children = new List<Shipping>();
        public override string Description => _description;
        public override DateTime Time => _time;
        public override Aggregate Records => _records;
        public override byte Stage => _stage;
        public override Shipping this[int index] => _children[index];

        public Process(Aggregate records = null, byte stage = 0)
        {
            _records = records;
            _stage = stage;
            _time = DateTime.Now;

            if (_records == null) _description = "The XYZ Shipping Company :: Dashboard";
            else _description = records.ToString().PadRight(13) + _time;
        }

        public override void DisplayInfo(bool timestamps, bool root = false)
        {
            // Displaying code...
        }

        public override void ListProducts()
        {
            byte select = Utensil.Prompt("Select Logistic from List"), Menu(), "Back");
            while (select != 0)
            {
                Console.WriteLine();
                _children[select - 1].Records.Display();
                select = Utensil.Prompt("Select Logistic from List", Menu(), "Back");
            }
        }

        public override void Add(Shipping item)
        {
            _children.Add(item);
        }

        public override void Remove(Shipping item)
        {
            _children.Remove(item);
        }

        public override int Count()
        {
            return _children.Count;
        }

        public override string[] Menu()
        {
            string[] list = new string[_children.Count];
            for (int i = 0; i < list.Length; i++) {
				list[i] = String.Format("{0}", _children[i].Description);
			}
            return list;
        }
    }
```  
<br/>
We define our **Shipment** class to be the Leaf of project.

```cs
    public class Shipment : Shipping
    {
        private string _description;
        private DateTime _time;
        private Aggregate _records;

        public Shipment(Aggregate records)
        {
            _time = DateTime.Now;
            _records = records;
            _description = records.ToString().PadRight(13) + _time;
        }

        public override string Description => _description;

        public override DateTime Time => _time;

        public override Aggregate Records => _records;

        public override void DisplayInfo(bool timestamps, bool root = false)
        {
            string title = String.Format("{0}", Description);
            Console.WriteLine();

            title = String.Format("{0} :: {1}\n", title, 8);
            title = String.Format("{0}{1}\n", title, "".PadRight(title.Length + 6, '.'));
            title = String.Format("{0} Weight {1} : Qty ", title, _records.Weigh());
            title = String.Format("{0} {1}\n", title, _records.Count());

            Console.WriteLine("{0} {1}\n", title, Utensil.STAGE[8 - 1]));
            Console.Write("{1}{2}",title,"".PadRight(Utensil.STAGE[8 - 1].Length + 1,'-'));
            Console.WriteLine();
        }
    }
```  

The **Leaf** is then used in dashboard to list an instance of shipping in list, and afford us the following display.

![Dashboard](https://cloud.owncube.com/apps/gallery/preview.public/60416322?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  

<br/><br/><br/>
### Proxy Pattern
This pattern defines a class to map our **Application Service** in the WCF.

```cs
    public class Subject : Base
    {
        protected ServiceReference1.ServiceClient theclient;
		theclient = new ServiceReference1.ServiceClient();

        Shipping list = new Process();

        public void Create()
        {
            Aggregate business = null;
            switch (Utensil.Prompt("Select Company", theclient.Prompt(), "Back"))
            {
                case 1: business = new ABC(); break;
                case 2: business = new DEF(); break;
                case 3: business = new GHI(); break;
				case 0: return; 
            }
            do
            {
                business.Add(Data());
            } while (Utensil.Prompt("Add Another?", new string[] { "Yes" }, "No") != 0);

            Shipment arrived = new Shipment(business);
            Process stage7 = new Process(business, 7);
            // all the way to stage 1...
            list.Add(stage1);
        }

        private Product Data()
        {
            // code prompting new Product object
        }

        public void GetList()
        {
            // code with conditions that might call list.DisplayInfo(false, true);
        }
        public void Query()
        {
			// code with condition that might call list.ListProducts();
        }
        public void TimeStamps()
        {
			// code selects a 'target' shipping instance and displays timestamps
        }

        public string[] Prompt()
        {
        	return theclient.Prompt();
        }
        public string[] Pavilion()
        {
        	return theclient.Pavilion();
        }
        public string[] AddMore()
        {
        	return theclient.AddMore();
        }

    }
```
The above is then injected to the following class, which is then used in the **Main** method. 

```cs
    public class Proxy : Base
    {
        Subject ledger;
        public Proxy(Subject ledger) {
            this.ledger = ledger;
        }
        public void Create() {
            if (ledger == null) ledger = new Subject();
            ledger.Create();
        }
        public void GetList() {
            // implementation simlar to above..
        }
        public void Query() {
            // implementation simlar to above..
        }
        public void TimeStamps() {
            if (ledger == null) ledger = new Subject();
            ledger.TimeStamps();
        }
    }
```

The following screenshot shows a timestamp log of a typical shipment

![Timestamps](https://cloud.owncube.com/apps/gallery/preview.public/60379972?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  

### Windows Communincation Foundation
By dictionary definition; an API serves backend services to clients. Due to a lack of a database server, however, this definition is not fit for this project.  
The WCF instead is used for lightweight operations like serving application required strings as follows.

```cs
    public class Service : IService
    {
        
      public string[] Stage()
        {
            string[] STAGE = new string[] {
            "Companies Packing Goods",
            "XYZ Receiving Goods",
            "XYZ Packing Goods to Ship",
            "Ship Leaving Source Harbour",
            "Ship Arriving at Destination",
            "Unpacking Goods at Destination",
            "Transporting to Receiving Ends",
            "Orderers Confirming Receipt"
    };
            return STAGE;

    }

        public string[] Pavillion()
        {
            string[] test = new string[3];
            test[0] = "System Dash";
            test[1] = "New Shipping";
            test[2] = "Targeted Query";
            //string s = string.Join(" ", test);
            return test;
        }
        public string[] Prompt()
        {
            string[] test = new string[3];
            test[0] = "ABC Products";
            test[1] = "DEF Foods";
            test[2] = "HIG Parts";
            //string s = string.Join(" ", test);
            return test;
        }
        public string[] AddMore()
        {
            string[] test = new string[2];
            test[0] = "Yes";
            test[1] = "No";
            
            //string s = string.Join(" ", test);
            return test;
        }
    } 
```  
<br/><br/><br/><br/>
<br/><br/><br/><br/>
### Application
Ultimately, the application has the following implementation.

```cs
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Title = "XYZ Shipping Co.";
            Proxy client = new Proxy(new Subject());

            byte option = Utensil.Pavilion();
            while (option != 0)
            {
                switch (option)
                {
                    case 1: client.GetList(); break;
                    case 2: client.Create(); break;
                    case 3: client.TimeStamps(); break;
                    case 4: client.Query(); break;
                }
                option = Utensil.Pavilion();
            }
            System.Console.Clear();
        }
    }
```
