using System;

namespace xyz
{
    public static class Utensil
    {
        public static readonly string[] STAGE = new string[] {
            "Companies Packing Goods",
            "XYZ Receiving Goods",
            "XYZ Packing Goods to Ship",
            "Ship Leaving Source Harbour",
            "Ship Arriving at Destination",
            "Unpacking Goods at Destination",
            "Transporting to Receiving Ends",
            "Orderers Confirming Receipt"
    };

        public static byte Pavilion()
        {
            string[] prompt = { "System Dash", "New Shipping", "TimeStamps", "Targeted Query" };
            return Utensil.Prompt("The XYZ Shipping Company", prompt, "Exit");
        }

        public static byte Capture()
        {
            string[] prompt = { "ABC Products", "DEF Foods", "GHI Parts" };
            return Utensil.Prompt("Select Business", prompt, "Back");
        }

        public static byte Prompt(string prompt, string[] options, string exit = null)
        {
            int val = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("{0}", prompt);
                Console.WriteLine("".PadRight(prompt.Length, '='));
                for (byte i = 1; i <= options.Length; i++) Console.WriteLine("- {0} :: {1}", i, options[i - 1]);

                if (exit != null)
                {
                    if (options.Length > 1) Console.WriteLine();
                    Console.WriteLine("- {0} :: {1}", 0, exit);
                    Console.WriteLine();
                }
                else Console.WriteLine();

                Console.Write("selection >> ");
                if (!IsInt(Console.ReadLine(), ref val)) continue;
                if (val >= (exit != null ? 0 : 1) && val <= options.Length) break;
            } while (true);
            return (byte)val;
        }

        public static double Digit(ref bool valid, string prompt)
        {
            double digit = 0;
            Console.Write(prompt);
            try
            {
                digit = double.Parse(Console.ReadLine());
                valid = true;
            }
            catch (System.FormatException)
            {
                valid = false;
                ConsoleColor temp = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid Input!");
                Console.ForegroundColor = temp;
                Console.WriteLine("Let's try again");
                Console.ReadKey();
                return 0;
            }
            return digit;
        }

        public static bool IsInt(string input, ref int val)
        {
            try
            {
                val = int.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
        public static bool IsFloat(string input, ref double val)
        {
            try
            {
                val = double.Parse(input);
            }
            catch (System.FormatException)
            {
                return false;
            }
            return true;
        }
    }
}
