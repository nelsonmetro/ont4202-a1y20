using System;
using System.Collections.Generic;

namespace xyz
{
    public class Process : Shipping
    {
        private string _description;
        private DateTime _time;
        private Aggregate _records;
        private byte _stage;

        private List<Shipping> _children = new List<Shipping>();

        public override string Description => _description;

        public override DateTime Time => _time;

        public override Aggregate Records => _records;

        public override byte Stage => _stage;

        public override Shipping this[int index] => _children[index];

        public Process(Aggregate records = null, byte stage = 0)
        {
            _records = records;
            _stage = stage;
            _time = DateTime.Now;

            if (_records == null) _description = "The XYZ Shipping Company :: Dashboard";
            else _description = records.ToString().PadRight(13) + _time;
        }

        public override void DisplayInfo(bool timestamps, bool root = false)
        {
            string title = String.Format("{0}", Description);
            if (root)
            {
                /* title = String.Format(" {0} | Total Cost :: {1:c}", Description.PadRight(15), Cost); */
                Console.WriteLine("{0}\n{1}", title, "".PadRight(title.Length, '='));
            }
            else
            {
                title = String.Format("{0} :: {1}\n{2}\n", title, _stage, "".PadRight(title.Length + 6, '.'));
                title = String.Format("{0} Weight {1}kg : Qty {2}\n", title, _records.Weigh(), _records.Count());
                if (timestamps || this.Stage == 8)
                {
                    Console.WriteLine();
                    Console.WriteLine("{0} {1}\n{2}", title, Utensil.STAGE[_stage - 1], "".PadRight(Utensil.STAGE[_stage - 1].Length + 1, '-'));
                }
            }

            foreach (var item in _children)
            {
                item.DisplayInfo(timestamps);
            }
        }

        public override void ListProducts()
        {
            byte select = Utensil.Prompt("Select Logistic from List".PadLeft(39), Menu(), "Back");
            while (select != 0)
            {
                Console.WriteLine();
                _children[select - 1].Records.Display();
                select = Utensil.Prompt("Select Logistic from List".PadLeft(39), Menu(), "Back");
            }
        }

        public override void Add(Shipping item)
        {
            _children.Add(item);
        }

        public override void Remove(Shipping item)
        {
            _children.Remove(item);
        }

        public override int Count()
        {
            return _children.Count;
        }

        public override string[] Menu()
        {
            string[] list = new string[_children.Count];
            for (int i = 0; i < list.Length; i++) list[i] = String.Format("{0}", _children[i].Description);
            return list;
        }
    }
}
