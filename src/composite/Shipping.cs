using System;

namespace xyz
{
    public abstract class Shipping
    {
        public abstract string Description { get; }
        public abstract DateTime Time { get; }
        public abstract Aggregate Records { get; }
        public virtual byte Stage { get; }

        public abstract void DisplayInfo(bool timestamps, bool root = false);

        public virtual Shipping this[int index]
        {
            get
            {
                throw new System.NotSupportedException("Sorry, not today.");
            }
        }

        public virtual void Add(Shipping item)
        {
            throw new System.NotSupportedException("Nothing to Add");
        }

        public virtual void Remove(Shipping item)
        {
            throw new System.NotSupportedException("Nothing to Remove");
        }

        public virtual int Count()
        {
            throw new System.NotSupportedException("Nothing to Count.");
        }

	public virtual void ListProducts()
	{
            throw new System.NotSupportedException("Nothing to See Here.");
	}

        public virtual string[] Menu()
	{
            throw new System.NotSupportedException("Nothing to See Here.");
	}

    }
}
