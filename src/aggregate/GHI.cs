namespace xyz
{
    public class GHI : Aggregate
    {

        public Product this[int index]
        {
            get { return _items[index]; }
        }

        public override void Add(Product o)
        {
		_items.Add(o);
        }

        public override Iterator CreateIterator()
        {
            return new IteratorGHI(this);
        }

	public override string ToString()
	{
		return "GHI Parts";
	}
    }
}
