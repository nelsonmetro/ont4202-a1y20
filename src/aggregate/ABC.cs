namespace xyz
{
    public class ABC : Aggregate
    {

        public Product this[int index]
        {
            get { return _items[index]; }
        }

        public override void Add(Product o)
        {
            _items.Add(o);
        }

        public override Iterator CreateIterator()
        {
            return new IteratorABC(this);
        }

	public override string ToString()
	{
		return "ABC Products";
	}
    }
}
