namespace xyz
{
    public class Proxy : Base
    {
        Subject ledger;

        public Proxy(Subject ledger)
        {
            this.ledger = ledger;
        }

        public void Create()
        {
            if (ledger == null) ledger = new Subject();
            ledger.Create();
        }

        public void GetList()
        {
            if (ledger == null) ledger = new Subject();
            ledger.GetList();
        }

        public void Query()
        {
            if (ledger == null) ledger = new Subject();
            ledger.Query();
        }

        public void TimeStamps()
        {
            if (ledger == null) ledger = new Subject();
            ledger.TimeStamps();
        }
    }
}
