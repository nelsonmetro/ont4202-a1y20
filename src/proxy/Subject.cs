using System;

namespace xyz
{
    public class Subject : Base
    {
        /* protected ServiceReference1.Service1Client theclient = new ServiceReference1.Service1Client(); */
        Shipping list = new Process();

        public void Create()
        {
            Aggregate business = null;
            switch (Utensil.Prompt("Select Company", new string[] { "ABC Products", "DEF Foods", "GHI Parts" }, "Back"))
            {
                case 1: business = new ABC(); break;
                case 2: business = new DEF(); break;
                case 3: business = new GHI(); break;
		case 0: return; 
            }
            do
            {
                business.Add(Data(business.ToString()));
            } while (Utensil.Prompt("Add Another?", new string[] { "Yes" }, "No") != 0);

            Shipment arrived = new Shipment(business);
            Process stage7 = new Process(business, 7);
            stage7.Add(arrived);
            Process stage6 = new Process(business, 6);
            stage6.Add(stage7);
            Process stage5 = new Process(business, 5);
            stage5.Add(stage6);
            Process stage4 = new Process(business, 4);
            stage4.Add(stage5);
            Process stage3 = new Process(business, 3);
            stage3.Add(stage4);
            Process stage2 = new Process(business, 2);
            stage2.Add(stage3);
            Process stage1 = new Process(business, 1);
            stage1.Add(stage2);

            list.Add(stage1);
        }
        private Product Data(string business)
        {
            Console.Clear();
            string prompt = String.Format("Shipping Products for {0}", business);
            Console.WriteLine("{0}\n{1}\n", prompt, "".PadRight(prompt.Length, '='));

            Console.Write("{0,-17}: ", "Item Description");
            string desc = Console.ReadLine();
            Console.Write("{0,-17}: ", "Item Weight (kg)");
            double weight = double.Parse(Console.ReadLine());

            return new Product(desc, weight);
        }

        public void GetList()
        {
            switch (list.Count())
            {
                case 0: Console.WriteLine("Nothing to See here!"); break;
                default:
                    Console.Clear();
                    list.DisplayInfo(false, true);
                    break;
            }
            Console.ReadKey();
        }

        public void Query()
        {
            if (list.Count() == 0)
            {
                Console.WriteLine("Nothing to See here!");
                Console.ReadKey();
                return;
            }
            list.ListProducts();
        }

        public void TimeStamps()
        {
            switch (list.Count())
            {
                case 0: Console.WriteLine("Nothing to See here!"); break;
                default:
                    Console.Clear();
                    byte select = Utensil.Prompt("Select Logistic from List".PadLeft(39), list.Menu(), "Back");
		    if(select == 0) return;
		    Shipping target = list[select - 1];
                    string title = "The XYZ Shipping Company :: TimeStamps";
                    Console.WriteLine("{0}\n{1}", title, "".PadRight(title.Length, '='));
                    target.DisplayInfo(true);
                    break;
            }
            Console.ReadKey();
        }

        /* public string[] Prompt() */
        /* { */
        /* 	return theclient.Prompt(); */
        /* } */
        /* public string[] Pavilion() */
        /* { */
        /* 	return theclient.Pavilion(); */
        /* } */
        /* public string[] AddMore() */
        /* { */
        /* 	return theclient.AddMore(); */
        /* } */

    }
}
