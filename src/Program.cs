﻿namespace xyz
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.Title = "XYZ Shipping Co.";
            Proxy focus = new Proxy(new Subject());

            byte choice = Utensil.Pavilion();
            while (choice != 0)
            {
                switch (choice)
                {
                    case 1: focus.GetList(); break;
                    case 2: focus.Create(); break;
                    case 3: focus.TimeStamps(); break;
                    case 4: focus.Query(); break;
                }
                choice = Utensil.Pavilion();
            }
            System.Console.Clear();
        }
    }
}
