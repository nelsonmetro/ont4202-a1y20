namespace xyz
{
    public interface Iterator
    {
        Product First();
        Product Next();
        Product Current();
        bool IsDone();
    }
}
